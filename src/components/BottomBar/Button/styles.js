import styled from 'styled-components/native';

import colors from '../../colors';

const TouchableOpacity = styled.TouchableOpacity`
  flex: 1;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  padding: 10px;
  background-color: ${props => (props.active ? colors.bg : colors.bgDark)};
`;

export default TouchableOpacity;
