import styled from 'styled-components/native';

import colors from '../colors';

const BottomBar = styled.View`
  flex-direction: row;
  height: 100px;
  width: 100%;
  background-color: ${colors.bgDark};
`;

export default BottomBar;
