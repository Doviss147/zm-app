import styled from 'styled-components/native';
import VectorIcon from 'react-native-vector-icons/FontAwesome';

import colors from './colors';

const View = styled.View`
  background-color: ${props => props.backgroundColor || colors.bg};
  height: 100%;
  width: 100%;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.View`
  background-color: ${props => props.backgroundColor || colors.bg};
  align-items: center;
  justify-content: center;
`;

const AppWrapper = styled.View`
  background-color: ${colors.bg};
  padding-top: 25px;
  flex: 1;
`;

const SwiperDots = styled.View`
  background-color: rgba(0, 0, 0, 0.2); 
  width: 30px; 
  height: 10px;
  border-radius: 4px; 
  margin: 3px;
`;

const ActiveDot = styled(SwiperDots)`
  background-color: ${colors.bgElement}; 
  width: 10px;
`;

const Text = styled.Text`
  color: ${colors.text};
  font-size: ${props => props.size || 14};
`;

const TouchableOpacity = styled.TouchableOpacity`
  width: 100%;
  background-color: ${props => props.color || colors.bgElement};
  padding: 15px;
  border-bottom-right-radius: ${props => (typeof props.rightRadius === 'undefined' ? 5 : props.rightRadius)};
  border-top-right-radius: ${props => (typeof props.rightRadius === 'undefined' ? 5 : props.rightRadius)};
  border-bottom-left-radius: ${props => (typeof props.leftRadius === 'undefined' ? 5 : props.leftRadius)};
  border-top-left-radius: ${props => (typeof props.leftRadius === 'undefined' ? 5 : props.leftRadius)};
  border-bottom-width: 2px;
  border-bottom-color: ${props => props.bottomColor || colors.bgElementDark};
  align-items: center;
  justify-content: center;
`;

const Image = styled.Image`
  margin: 15px;
  width: 150px;
  height: 150px;
  border-color: rgba(0, 0, 0, 0.2);
  border-width: 5px;
  border-radius: 150px;
`;

const Icon = styled(VectorIcon)`
  width: 25px;
  height: 25px;
`;

export {
  View,
  Wrapper,
  AppWrapper,
  Text,
  TouchableOpacity,
  Image,
  Icon,
  SwiperDots,
  ActiveDot,
};
