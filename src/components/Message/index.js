import React from 'react';
import PropTypes from 'prop-types';

import colors from '../colors';
import { Text, Icon } from '../styles';
import { Wrapper, TextWrapper, IconWrapper } from './styles';

const Component = ({ danger = false, text, onPress }) => (
  <Wrapper onPress={onPress}>
    <TextWrapper danger={danger}><Text size={16}>{text}</Text></TextWrapper>
    <IconWrapper><Icon name="times-circle" size={24} color={colors.bgElement} /></IconWrapper>
  </Wrapper>
);

export default Component;

Component.propTypes = {
  danger: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};
