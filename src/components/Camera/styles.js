import styled from 'styled-components/native';

import colors from '../colors';

const opacity = 'rgba(0, 0, 0, .6)';

const TopLayer = styled.View`
  background-color: ${opacity};
  flex-basis: 20%;
`;

const CenterLayer = styled.View`
  display: flex;
  flex-basis: 60%;
  flex-direction: row;
  justify-content: space-between;
`;

const BottomLayer = styled.View`
  background-color: ${opacity};
  flex-basis: 20%;
`;

const SideLayer = styled.View`
  background-color: ${opacity};
  flex-basis: 10%;
`;

const FocusLayer = styled.View`
  display: flex;
  flex-basis: 80%;
  flex-direction: row;
  justify-content: space-between;
`;

const CornerContainer = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Corner = styled.View`
  height: 50px;
  width: 50px;
  border-color: ${colors.bgElement};
  border-style: solid;
  border-top-left-radius: ${props => (props.leftBorder && props.topBorder ? 15 : 0)};
  border-top-right-radius: ${props => (props.rightBorder && props.topBorder ? 15 : 0)};
  border-bottom-left-radius: ${props => (props.leftBorder && props.bottomBorder ? 15 : 0)};
  border-bottom-right-radius: ${props => (props.rightBorder && props.bottomBorder ? 15 : 0)};
  border-left-width: ${props => (props.leftBorder ? 4 : 0)};
  border-right-width: ${props => (props.rightBorder ? 4 : 0)};
  border-top-width: ${props => (props.topBorder ? 4 : 0)};
  border-bottom-width: ${props => (props.bottomBorder ? 4 : 0)};
`;

export {
  TopLayer,
  CenterLayer,
  BottomLayer,
  SideLayer,
  FocusLayer,
  CornerContainer,
  Corner,
};
