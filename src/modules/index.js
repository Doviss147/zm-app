import { combineReducers } from 'redux';

import user from './user';
import calendars from './calendars';
import events from './events';
import messages from './messages';

export const actions = {
  user: user.actions,
  events: events.actions,
  calendars: calendars.actions,
  messages: messages.actions,
};

export const reducers = combineReducers({
  user: user.reducers,
  calendars: calendars.reducers,
  events: events.reducers,
  messages: messages.reducers,
});
