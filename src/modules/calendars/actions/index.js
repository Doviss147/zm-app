import { api } from '../../../utils';
import TYPES from './types';
import load from './load';

export { TYPES };

export default {
  load: load(api),
};
