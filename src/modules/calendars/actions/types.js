import { api } from '../../../utils';

export default {
  ROOMS: api.ACTION_TYPES('ROOMS'),
};
