import { TYPES } from '../actions';

export const initialState = {
  data: {},
  fetching: false,
  error: null,
};

const actions = ({
  [TYPES.ROOMS.REQUEST]: state => ({
    ...state,
    fetching: true,
    error: null,
  }),
  [TYPES.ROOMS.SUCCESS]: (state, payload) => ({
    data: payload,
    fetching: false,
    error: null,
  }),
  [TYPES.ROOMS.FAILURE]: (state, payload) => ({
    ...state,
    fetching: false,
    error: payload,
  }),
});

const reducer = (state = initialState, action) => (
  actions[action.type] ? actions[action.type](state, action.payload) : state
);

export default reducer;
