import reducer, { initialState } from './list';
import { TYPES } from '../actions';

const loadedState = {
  data: [
    {
      busy: [{
        start: '2019-01-07T14:00:37Z',
        end: '2019-01-07T14:30:00Z',
      }],
    },
    {
      free: [{
        start: '2019-01-07T14:30:00Z',
        end: '2019-01-07T16:30:00Z',
      }],
    },
    { room: { id: 'domain_numberId_0@resource.calendar.google.com' } },
  ],
  fetching: false,
  error: null,
};

describe('Rooms reducer', () => {
  it('should update state on REQUEST type correctly, on a loaded state', async () => {
    const payload = undefined;
    expect(reducer(loadedState, {
      type: TYPES.REQUEST,
      payload,
    }))
      .toEqual({
        ...loadedState,
        fetching: false,
        error: null,
      });
  });
  it('should update state on REQUEST type correctly, on initialState', async () => {
    const payload = undefined;
    expect(reducer(initialState, {
      type: TYPES.REQUEST,
      payload,
    }))
      .toEqual({
        ...initialState,
        fetching: false,
        error: null,
      });
  });

  it('should update state on FAILURE correctly', async () => {
    const payload = 'Error message';
    expect(reducer(loadedState, {
      type: TYPES.FAILURE,
      payload,
    }))
      .toEqual({
        ...loadedState,
        fetching: false,
        error: null,
      });
  });

  it('should update state on SUCCESS correctly', async () => {
    //const payload = loadedState.data;
    const payload = [];
    expect(reducer(loadedState, {
      type: TYPES.SUCCESS,
      payload,
    }))
      .toEqual({
        data: payload,
        fetching: false,
        error: null,
      });
    expect(reducer(initialState, {
      type: TYPES.SUCCESS,
      payload: [],
    }))
      .toEqual({
        data: [],
        fetching: false,
        error: null,
      });
  });
});
