import TYPES from './types';
import authenticate from '../authentication';
import signIn from './signIn';
import signOut from './signOut';
import updateToken from './updateToken';
import { api } from '../../../utils';

export { TYPES };

export default {
  signIn: signIn(authenticate),
  signOut,
  updateToken: updateToken(api),
};
