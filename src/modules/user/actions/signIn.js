import TYPES from './types';
import { TYPES as MESSAGE_TYPES } from '../../messages';
import { handleError } from '../../../utils';

const action = authenticate => navigation => async (dispatch) => {
  dispatch({ type: TYPES.SIGN_IN.REQUEST });
  try {
    const result = await authenticate();
    if (result.type === 'success') {
      dispatch({ type: TYPES.SIGN_IN.SUCCESS, payload: result });
      // TODO: move navigation out from modules
      navigation.navigate('App');
    } else {
      dispatch({ type: TYPES.SIGN_OUT });
      dispatch({ type: MESSAGE_TYPES.INFO, payload: 'Authentication has been cancelled.' });
    }
  } catch (error) {
    dispatch({ type: TYPES.SIGN_IN.FAILURE, payload: handleError(error) });
    dispatch({ type: MESSAGE_TYPES.ERROR, payload: handleError(error) });
  }
};

export default action;
