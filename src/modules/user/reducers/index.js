import { TYPES } from '../actions';

const initialState = {
  data: null,
  fetching: false,
  error: null,
};

const actions = ({
  [TYPES.SIGN_IN.REQUEST]: state => ({
    ...state,
    fetching: true,
    error: null,
  }),
  [TYPES.SIGN_IN.SUCCESS]: (state, payload) => ({
    data: payload,
    fetching: false,
    error: null,
  }),
  [TYPES.SIGN_IN.FAILURE]: (state, payload) => ({
    ...state,
    fetching: false,
    error: payload,
  }),
  [TYPES.SIGN_OUT]: () => ({
    ...initialState,
  }),
  [TYPES.UPDATE_TOKEN.REQUEST]: state => ({
    ...state,
    fetching: true,
    error: null,
  }),
  [TYPES.UPDATE_TOKEN.SUCCESS]: (state, payload) => ({
    data: { ...state.data, ...payload },
    fetching: false,
    error: null,
  }),
  [TYPES.UPDATE_TOKEN.FAILURE]: (state, payload) => ({
    ...state,
    fetching: false,
    error: payload,
  }),
});

const reducer = (state = initialState, action) => (
  actions[action.type] ? actions[action.type](state, action.payload) : state
);

export default reducer;
