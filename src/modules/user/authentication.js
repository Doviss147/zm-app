import { Google } from 'expo';
import { security } from '../../config';

const authenticate = async () => Google.logInAsync({
  androidClientId: security.auth.android ? security.auth.android.clientId : undefined,
  iosClientId: security.auth.ios ? security.auth.ios.clientId : undefined,
  scopes: [
    security.permissions.VIEW_PROFILE,
    security.permissions.VIEW_EMAIL,
    security.permissions.VIEW_EDIT_EVENTS,
    security.permissions.VIEW_CALENDARS,
  ],
  behavior: security.auth.behavior,
});

export default security.mock ? () => security.mock : authenticate;
