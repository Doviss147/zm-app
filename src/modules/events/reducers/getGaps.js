import moment from 'moment';

import config from '../../../config';

const formatTime = (dateTime, floor) => {
  const time = moment(dateTime);

  let minutes;
  if (floor) minutes = Math.floor(parseInt(time.format('mm'), 10) / config.minGapDuration) * config.minGapDuration;
  else minutes = Math.ceil(parseInt(time.format('mm'), 10) / config.minGapDuration) * config.minGapDuration;

  if (minutes === 60) {
    time.add(1, 'hour');
    time.set({ minute: 0, millisecond: 0 });
  } else {
    time.set({ minute: minutes, millisecond: 0 });
  }
  return time.toISOString();
};

const getGaps = (data, from = formatTime(moment(), true), to = moment(config.maxTime, 'HH:mm').toISOString()) => {
  const gaps = [];
  const { items } = data;
  let gapStart = from;
  items.forEach(({ end, start }) => {
    gaps.push({ start: gapStart, end: formatTime(start.dateTime, true) });
    gapStart = formatTime(end.dateTime, false);
  });
  gaps.push(items.length ? {
    start: formatTime(items[items.length - 1].end.dateTime, false),
    end: formatTime(to, true),
  } : { start: from, end: to });

  return gaps.filter(gap => moment(gap.end).isAfter(moment(from)))
    .filter(gap => moment(gap.start).isBefore(moment(gap.end)));
};

export default getGaps;
