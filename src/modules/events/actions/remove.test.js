import action from './remove';
import TYPES from './types';

let dispatch;

const params = ['calendar@google.com', 'we457w874q87'];

const apiSuccess = {
  execute: () => ({
    roomId: params[0],
    id: params[1],
  }),
  endpoints: {
    deleteEvent: () => {},
  },
};

const apiMissingEvent = {
  execute: () => {
    throw ({
      response: {
        status: 410,
      },
    });
  },
  endpoints: {
    deleteEvent: () => {},
  },
};

const apiFailure = {
  execute: () => {
    throw ({
      response: {
        status: 400,
      },
    });
  },
  endpoints: {
    deleteEvent: () => {},
  },
};

describe('Event deletion action creator', () => {
  beforeEach(() => {
    dispatch = jest.fn();
  });

  it('should dispatch DELETE.SUCCESS action', async () => {
    await action(apiSuccess)(params[0], params[1])(dispatch);
    expect(dispatch).toHaveBeenNthCalledWith(
      1,
      { type: TYPES.DELETE.REQUEST, payload: { roomId: params[0] } },
    );
    expect(dispatch).toHaveBeenNthCalledWith(
      2,
      { type: TYPES.DELETE.SUCCESS, payload: { roomId: params[0], eventId: params[1] } },
    );
  });

  it('should dispatch DELETE.FAILURE action', async () => {
    await action(apiFailure)(params[0], params[1])(dispatch);
    expect(dispatch).toHaveBeenNthCalledWith(
      1,
      { type: TYPES.DELETE.REQUEST, payload: { roomId: params[0] } },
    );
    expect(dispatch).toHaveBeenNthCalledWith(
      2,
      { type: TYPES.DELETE.FAILURE, payload: { error: { response: { status: 400 } }, roomId: params[0] } },
    );
  });

  it('should dispatch DELETE.SUCCESS if event is already deleted', async () => {
    await action(apiMissingEvent)(params[0], params[1])(dispatch);
    expect(dispatch).toHaveBeenNthCalledWith(
      1,
      { type: TYPES.DELETE.REQUEST, payload: { roomId: params[0] } },
    );
    expect(dispatch).toHaveBeenNthCalledWith(
      2,
      { type: TYPES.DELETE.SUCCESS, payload: { roomId: params[0], eventId: params[1] } },
    );
  });
});
