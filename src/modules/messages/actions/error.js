import { handleError } from '../../../utils';
import TYPES from './types';

const action = error => (dispatch) => {
  dispatch({
    type: TYPES.ERROR,
    payload: handleError(error),
  });
};

export default action;
