import action from './info';
import TYPES from './types';

const dispatch = jest.fn();

describe('Messages error action creator', () => {
  it('should dispatch INFO action with specified message in payload', async () => {
    action('info')(dispatch);
    expect(dispatch).toBeCalledWith({ type: TYPES.INFO, payload: 'info' });
  });
});
