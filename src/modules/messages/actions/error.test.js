import action from './error';
import TYPES from './types';

const dispatch = jest.fn();

describe('Messages error action creator', () => {
  it('should dispatch ERROR action with the same message if error is a plain string', async () => {
    action('error')(dispatch);
    expect(dispatch).toBeCalledWith({ type: TYPES.ERROR, payload: 'error' });
  });

  it('should dispatch ERROR action with extracted message property if error is an object', async () => {
    action({ message: 'error' })(dispatch);
    expect(dispatch).toBeCalledWith({ type: TYPES.ERROR, payload: 'error' });
  });

  it('should dispatch ERROR action with extracted message property from response'
    + ' if error is API response object', async () => {
    action({ data: { message: 'error' } })(dispatch);
    expect(dispatch).toBeCalledWith({ type: TYPES.ERROR, payload: 'error' });
  });
});
