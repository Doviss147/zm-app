import TYPES from './types';

const action = () => (dispatch) => {
  dispatch({ type: TYPES.CLEAR });
};

export default action;
