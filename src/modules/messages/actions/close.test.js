import action from './close';
import TYPES from './types';

const dispatch = jest.fn();

describe('Messages close action creator', () => {
  it('should dispatch REMOVE_INFO action', async () => {
    action({ info: true })(dispatch);
    expect(dispatch).toBeCalledWith({ type: TYPES.REMOVE_INFO, payload: 0 });
  });

  it('should dispatch REMOVE_ERROR action with specified error index in payload', async () => {
    action({ idx: 2 })(dispatch);
    expect(dispatch).toBeCalledWith({ type: TYPES.REMOVE_ERROR, payload: 2 });
  });
});
