import React from 'react';
import PropTypes from 'prop-types';

import ViewComponent from '../views/Login';

const Screen = ({ navigation }) => <ViewComponent navigation={navigation} />;

Screen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default Screen;
