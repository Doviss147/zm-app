import Calendar from './Calendar';
import Scanner from './Scanner';
import Login from './Login';

export default {
  Calendar,
  Scanner,
  Login,
};
