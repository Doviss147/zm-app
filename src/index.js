import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { registerRootComponent } from 'expo';

import App from './App';
import { store, persistor } from './store';

const RootComponent = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>
);

registerRootComponent(RootComponent);
