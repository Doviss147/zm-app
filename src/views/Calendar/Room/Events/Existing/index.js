import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import PropTypes from 'prop-types';

import { actions } from '../../../../../modules';
import View from './view';

class Component extends React.Component {
  handleRemove = id => () => {
    this.props.dispatch(actions.events.remove(this.props.room.id, id));
  };

  handlers = {
    remove: this.handleRemove,
  };

  render() {
    return !!this.props.data.length && (
      <View {...this.props} handlers={this.handlers} />
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  data: state.events.list.data[ownProps.room.id].items
    .filter(event => (event.organizer && (event.organizer.email === state.user.data.user.email)
        && moment(event.end.dateTime).isAfter(moment()))),
});

export default connect(mapStateToProps)(Component);

Component.propTypes = {
  room: PropTypes.object.isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  dispatch: PropTypes.func.isRequired,
};
