import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { View, ListItem } from 'native-base';

import colors from '../../../../../components/colors';
import { Text, Icon } from '../../../../../components/styles';
import { Wrapper, TextWrapper, IconWrapper } from './styles';

const formatTime = time => moment(time).format('HH:mm');
const getDuration = (from, to) => moment.utc(moment(to).diff(moment(from))).format('HH:mm');

const ComponentView = ({ data, handlers }) => (
  <View style={{ width: '100%' }}>
    {data.map(event => (
      <ListItem key={event.id} style={{ borderBottomColor: 0 }}>
        <Wrapper onPress={handlers.remove(event.id)}>
          <TextWrapper>
            <Text size={20}>{formatTime(event.start.dateTime)} - {formatTime(event.end.dateTime)}</Text>
            <Text size={16}>{getDuration(event.start.dateTime, event.end.dateTime)}</Text>
          </TextWrapper>
          <IconWrapper><Icon name="trash-o" size={24} color={colors.bgElement} /></IconWrapper>
        </Wrapper>
      </ListItem>
    ))}
  </View>
);

export default ComponentView;

ComponentView.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  handlers: PropTypes.shape({
    remove: PropTypes.func.isRequired,
  }).isRequired,
};
