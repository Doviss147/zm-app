import React from 'react';
import PropTypes from 'prop-types';
import Swiper from 'react-native-swiper';
import { Container } from 'native-base';
import { Dimensions } from 'react-native';

import Message from '../Message';
import Room from './Room';
import { View, Wrapper, Text, SwiperDots, ActiveDot } from '../../components/styles';

const window = Dimensions.get('window');

const Component = ({ calendars, setSwiper, ...props }) => (
  <Container>
    <Container>
      <View>
        {calendars.fetching ? (
          <Text size={20}>Wait a moment...</Text>
        ) : (Object.keys(calendars.data).length ? (
          <Swiper
            ref={setSwiper}
            dot={<SwiperDots />}
            activeDot={<ActiveDot />}
            height={window.height}
            width={window.width}
          >
            {Object.keys(calendars.data).map(key => (
              <Room key={key} id={key} {...props} />
            ))}
          </Swiper>
        ) : (
          <Wrapper>
            <Text size={20}>Sorry, no available rooms for today.</Text>
            <Text size={20} />
            <Text size={14}>Make sure you have some rooms available</Text>
            <Text size={14}>as resources in google calendar.</Text>
          </Wrapper>
        ))}
      </View>
    </Container>
    <Message />
  </Container>
);

export default Component;

Component.propTypes = {
  calendars: PropTypes.object.isRequired,
  setSwiper: PropTypes.func.isRequired,
};
