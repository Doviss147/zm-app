import PropTypes from 'prop-types';
import React from 'react';
import { View, Text, Button, Icon } from 'native-base';

import styles from './styles';

const ViewComponent = ({ code, valid = true, handleCancel }) => code && (
  <View style={[styles.bottomBarView, !valid && styles.bottomBarInvalid]}>
    <Text style={styles.itemText}>{!valid && 'Calendar not found for room:\n'}{code}</Text>
    <Button style={styles.itemButton} danger rounded small onPress={handleCancel}>
      <Icon name="md-close" />
    </Button>
  </View>
);

ViewComponent.propTypes = {
  code: PropTypes.string,
  valid: PropTypes.bool,
  handleCancel: PropTypes.func.isRequired,
};

export default ViewComponent;
