import { Permissions } from 'expo';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import View from './view';

class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      enabled: true,
      code: null,
      valid: true,
      hasCameraPermission: null,
    };
    // component does not dismount on navigation ir react native, we have to do this manually
    this.props.navigation.addListener('willFocus', () => this.setState({ enabled: true }));
    this.props.navigation.addListener('willBlur', () => this.setState({ enabled: false }));
  }

  async componentDidMount() {
    await this.requestCameraPermission();
  }

  requestCameraPermission = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === 'granted',
    });
  };

  handleSubmitBarcode = (result) => {
    if (result.data !== this.state.code) {
      const names = Object.keys(this.props.rooms).map(key => this.props.rooms[key].summary);
      const valid = names.includes(result.data);
      if (valid) {
        this.props.navigation.navigate('Calendar', { code: result.data });
      }
      this.setState({ code: result.data, valid });
    }
  };

  handleCancel = () => {
    this.setState({ code: null, valid: true });
  };

  handlers = {
    submitBarcode: this.handleSubmitBarcode,
    cancel: this.handleCancel,
  };

  render() {
    return this.state.enabled && (
      <View {...this.state} handlers={this.handlers} />
    );
  }
}

const mapStateToProps = state => ({
  rooms: state.calendars.data,
});

export default connect(mapStateToProps)(Component);

Component.propTypes = {
  rooms: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    addListener: PropTypes.func.isRequired,
  }).isRequired,
};
