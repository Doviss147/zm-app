import security from './security';

const config = {
  minMembers: 2,
  minGapDuration: 15,
  maxGapDuration: 2 * 60,
  maxTime: '20:00',
  durations: [
    {
      value: 15,
      label: '15 min',
    },
    {
      value: 30,
      label: '30 min',
    },
    {
      value: 60,
      label: '1 hr',
    },
  ],
  timeFormat: 'HH:mm',
};

export { security };

export default config;
