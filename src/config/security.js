// import envConfig from 'react-native-config';
import envConfig from '../../env.local';

const permissions = {
  VIEW_PROFILE: 'profile',
  VIEW_EMAIL: 'email',
  VIEW_EVENTS: 'https://www.googleapis.com/auth/calendar.events.readonly',
  VIEW_EDIT_EVENTS: 'https://www.googleapis.com/auth/calendar.events',
  VIEW_CALENDARS: 'https://www.googleapis.com/auth/calendar.readonly',
  VIEW_EDIT_SHARE_DELETE_CALENDARS: 'https://www.googleapis.com/auth/calendar',
  VIEW_CALENDAR_SETTINGS: 'https://www.googleapis.com/auth/calendar.settings.readonly',
};

export default {
  ...envConfig,
  permissions,
};
